# Simple Random Sentence Generator
- This little project is just a proof of concept. You are however welcome to use it, if you manage to find some use in it.

## Basic Sentence Structure
- Subject + Transitive Verb + Direct Object + Adverb + Preposition + Indirect Object

## Word Choice
- Each part of speech will be pulled from a pool of 15 words.
- Nouns (subject, objects) will share a common pool, and repetition will be allowed.
	- Nouns will also pull from the pool of standard modern English pronouns.
